(function(){

      var iframe = document.createElement("iframe");
      iframe.id = "YDChatWidget"
      iframe.src =  "ChatBot/index.html"
      iframe.style.width = '290px';
      iframe.style.height = 'auto';
      iframe.style.minHeight = '402px';
      iframe.style.position = 'fixed';
      iframe.style.border = '0';
      iframe.style.bottom = '1px';

      var div = document.createElement('div');
      div.id = 'YDCEmbeddedWidget'
      div.style.position = 'fixed';
      div.style.width = '290px';
      div.style.height = 'auto';
      div.style.minHeight = '402px';
      div.style.zIndex = '999';
      div.style.left = '100px';
      div.style.bottom = '0';
      div.style.outline = 'none ';
      div.style.visibility = 'visible ';
      div.style.resize = 'none ';
      div.style.overflow = 'visible ';
      div.style.opacity = '1 ';
      div.style.position = 'fixed ';
      div.style.border = '0px ';
      div.style.padding = '0px ';
      div.style.cursor = 'auto ';
      div.style.float = 'none ';
      div.style.transform = 'rotate(0deg) translateZ(0px) ';
      div.style.margin = '0px ';
      div.style.display = 'block ';
      div.style.background = 'none transparent ';

      div.appendChild( iframe );
      document.getElementsByTagName('body')[0].appendChild( div );
})();