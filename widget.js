var YDChatWidget = (function($, window){

  	var YDChatBot = {

  		chatId : '',
  		botImg : '',
  		botName : '',
  		database : '',
  		userImg : 'https://d1hny4jmju3rds.cloudfront.net/avatar/avatar7.png',
  		messagesRef : '',
  		fbId : '',
  		gplusId : '',
  		attempts : 0,

    	init : function(app, fbconf, bconf){

    		var self = this;
    		$(".chat-bot").show()
    		YDChatApiHandler.init(app);
    		self.fbId = app.fbId
    		self.gplusId = app.gplusId;
    		self.botImg = bconf.imageUrl;
    		self.botName = bconf.name;
    		self.bindEvents()
    		firebase.initializeApp(fbconf);
    	},

    	events:{

    		"click .min-header" : "maximiseChatBot",
    		"click .cb-min" : "minimiseChatBot",
    		"click .purple-bubble" : "sendResponse",
    		"click .YD_LOGIN" : "showUsername",
    		"click .FB_LOGIN" : "fbLogin",
    		"click .cbh-login" : "cbLogin",
    		"click .cbh-signup" : "sysSignup"
    	},

    	maximiseChatBot : function(e){

			$(".chat-bot").removeClass("min")
			YDChatBot.createSession()
    	},
    	createSession : function(){

    		var self = this;
    		var userId = '';
    		var isLoggedIn = false;
    		if(YDChatApiHandler.getUserId()){

    			userId = JSON.parse(YDChatApiHandler.getUserId()).id
    		}
    		if(YDChatApiHandler.getIsLoggedIn()){

    			isLoggedIn = true
    		}
    		var options = {}
    		options.url = YDChatApiHandler.nodeUrl+"/createBotSession",
    		options.method = "POST"
    		$.ajax({
	          	url : options.url,
	          	method : options.method,
	          	data:JSON.stringify({'isLoggedIn':isLoggedIn,'userId':userId}),
	          	contentType: "application/json"
	        }).done(function(res){
	            
	            console.log("chat Id generated is ", res)
	           	YDChatBot.chatId = res;	
	            YDChatBot.initiateChat()             
	        }).fail(function(error){

	          	console.log(error) ;
	        });
    	},
    	initiateChat : function(){

    		YDChatBot.initFireBase()
    	},
    	initFireBase : function(){

    		YDChatBot.database = firebase.database()
    		YDChatBot.loadMessages()
    	},
    	loadMessages : function(){

    		this.messagesRef = YDChatBot.database.ref('messages/'+YDChatBot.chatId);
			this.messagesRef.off();

			var setMessage = function(data) {

				var val = data.val();
			    YDChatBot.displayMessage(val);
			}
			  
			this.messagesRef.on('child_added', setMessage);
			this.messagesRef.limitToLast(10).on('child_changed', setMessage);
    	},
    	displayMessage : function(options){

    		if( typeof options.response == "object"){

    			var res = options.response;
    			res.forEach(function(item){

    				if(item.type == "bubble" && !item.action){

    					$(".chat-content").append('<div class="chat-txt actionable '+options.from+'"><div class="purple-bubble bubble" data-feeling="'+item.data+'">'+item.data+'</div></div>')
    				}
    				if(item.type == "bubble" && item.action){

    					$(".chat-content").append('<div id="'+item.action+'" class="actionable chat-txt '+options.from+'"><div class="p-action bubble '+item.action+'" data-feeling="'+item.data+'">'+item.data+'</div></div>')
    					if(item.action == "G_LOGIN")  YDChatBot.attachGoogle()
    					if(item.action == "FB_LOGIN") YDChatBot.initFacebook()
    				}
    				if(item.type == "text" && !item.action){

    					$(".chat-content").append('<div class="chat-txt bot"><img src="'+YDChatBot.botImg+'"><div class="white-bubble">'+item.data+'</div></div>')
    				}
    				if(item.type == "text" && item.action == "SIGNUP"){

    					$(".chat-content").append('<div class="chat-txt bot"><img src="'+YDChatBot.botImg+'"><div class="white-bubble">'+item.data+'</div></div>')
    					YDChatBot.showSignup()
    				}
    			})
    		}else{

    			if(options.from == "Donna"){

    				$(".chat-content").append('<div class="chat-txt bot"><img src="'+this.botImg+'"><div class="white-bubble">'+options.response+'</div></div>')
    			}else{

    				$(".chat-content").append('<div class="chat-txt user"><img src="'+this.userImg+'"><div class="white-bubble">'+options.response+'</div></div>')
    			}
    		}

    		this.bindEvents()
    		$(".chat-content")[0].scrollTop = $(".chat-content")[0].scrollHeight;
    	},

    	initFacebook : function(){

    		window.fbAsyncInit = function() {
				FB.init({
			    	appId      : YDChatBot.fbId,
			    	cookie     : true,  
			    	xfbml      : true,  
			    	version    : 'v2.2' 
			  	});
			};
		    (function(d, s, id) {
		    	var js, fjs = d.getElementsByTagName(s)[0];
		    	if (d.getElementById(id)) return;
		    	js = d.createElement(s); js.id = id;
		    	js.src = "//connect.facebook.net/en_US/all.js";
		    	fjs.parentNode.insertBefore(js, fjs);
		    }(document, 'script', 'facebook-jssdk'));
    	},
    	attachGoogle : function(){

    		gapi.load('auth2', function(){

				if( typeof auth2 == 'undefined'){

					auth2 = gapi.auth2.init({

						client_id: YDChatBot.gplusId,
					});
				}

				YDChatBot.googleSignIn(document.getElementById('G_LOGIN'));
			});
    	},
    	googleSignIn: function(ele){

    		auth2.attachClickHandler(ele, {},
		        
		        function(googleUser) {

		       		window.googleInfo = googleUser ;
		        	var gUserID = googleUser.getBasicProfile().getId();
		        	var gUserEmail = googleUser.getBasicProfile().getEmail();
		        	var oauthToken = "" ;
					for( var key in window.googleInfo ){

						if( typeof window.googleInfo[key].access_token != 'undefined' ) {

							oauthToken = window.googleInfo[key].access_token ;
							console.log( window.googleInfo[key].access_token )
						}
					}
		        	var base64 = 'GPLUS '+btoa( gUserID+"__"+gUserEmail + ":" + oauthToken );
		        	YDChatBot.socialLogin(base64, 'GPLUS');
		        },function(error) {

		          	console.log(JSON.stringify(error, undefined, 2));
		        });
    	},
    	fbLogin : function(e){

    		FB.login(function(response){

	      		YDChatBot.fbCallback(response);
	    	}, { 
	    		scope: 'public_profile,email' 
	    	});
    	},
    	fbCallback : function(response){
		
			if (response.status === 'connected'){

			    FB.api('/me', function(response){

			      console.log('Successful login for: ' + response.name);
			    });
			    var base64 = "FB "+btoa( response.authResponse.userID + ":" + response.authResponse.accessToken );
	    		YDChatBot.socialLogin(base64, "FB");
			}else if (response.status === 'not_authorized'){

				console.log('Please log into this app.');
			}else{

				console.log('Please try again later. Some error occurred');
			}
		},
    	socialLogin : function(token, source){

    		var loginUrl = YDChatApiHandler.apiUrl+'/auth/login';
    		$.ajax({
                type: 'POST',
                url: loginUrl,
                dataType: "JSON",
                data: JSON.stringify({"shouldGenerateCookie": true,"ipSession": false}),
                contentType: "application/json; charset=utf-8",
                xhrFields: {

     				withCredentials: true
			    },
			    beforeSend :function(xhr){

			    	xhr.setRequestHeader( "Authorization", token );
			    },
			    statusCode:{
			    	417 : function(response){

						YDChatBot.showError(response, false)
			    	},
			    },
            }).done(function(info) {

            	var data = info.user
            	YDChatBot.setLoginUser(data)

            }).error(function(error){

				YDChatBot.showError(error, false)     	
            })
    	},
    	setLoginUser : function(data){

    		YDChatBot.attempts = 0;
    		window.userdata = data;
			localStorage.setItem("isLoggedIn", 1)
			localStorage.setItem("user", JSON.stringify(data))
			YDChatBot.saveMessage("Login successful")
			$.cookie("DCU", JSON.stringify(data), {domain: ".yourdost.com", path: '/'});
			var options = {

				from : "Donna",
				text : "You are Awesome"
			}
			YDChatBot.sendToFirebase(options)
    	},
    	showSignup : function(){

    		var emailForm = '<div class="cb-login"><div class="cb-pswd"><input type="email" placeholder="Email" name="email" id="cbh-email"><button class="cbh-signup signup">send</button></div></div>';
    		$(".chat-content").append(emailForm);
    		YDChatBot.bindEvents()
    		$(".chat-content")[0].scrollTop = $(".chat-content")[0].scrollHeight;
    	},
    	sysSignup : function(e){

    		var email =  $("#cbh-email").val().trim()
    		YDChatBot.sendResponse(e,email)
    	},
    	cbLogin : function(e){

    		if($(e.currentTarget).hasClass("login")){

    			var pswd = $(".chat-content #cbh-pswd").val().trim();
    			if(!pswd) return
    			var data = {

	    			text : "Password: *******",
	    			class : "cb-login",
	    			from : "user"
	    		}
				YDChatBot.sendToFirebase(data)
				YDChatApiHandler.setPassword(pswd)
				YDChatBot.loginSys()
    		}else{

	    		var username = $(".chat-content #cbh-uname").val().trim();
	    		if(!username) return
	    		var data = {

	    			text : "Username: "+username,
	    			class : "cb-login",
	    			from : "user"
	    		}
				YDChatBot.sendToFirebase(data)
				YDChatApiHandler.setUsername(username)
				setTimeout(function(){

					YDChatBot.showPassword()
				},1000)
			}
    	},
    	loginSys : function(){

    		var userNamePswd = YDChatApiHandler.getUserNamePswd()
    		if(!userNamePswd.split("-")[0] || !userNamePswd.split("-")[1]){

    			YDChatBot.showUsername()
    		}
    		var loginUrl = YDChatApiHandler.apiUrl+'/v2/users/login'
    		$.ajax({
				type: "POST",
				dataType: "JSON",
            	contentType: "application/json; charset=utf-8",
    			url: loginUrl ,
    			xhrFields: {

     				withCredentials: true
			    },
			   	beforeSend :function(xhr){

			    	xhr.setRequestHeader( "Authorization", "Basic "+ btoa(userNamePswd.split("-")[0] + ":" + userNamePswd.split("-")[1]) );
			    },
			    statusCode:{
			    	401 : function(response){

			    		YDChatBot.showError(response, true)
			    	},
			    },
    			data:JSON.stringify({"shouldGenerateCookie": true,"ipSession": false})
			}).done(function( data, status, xhr ) {

				YDChatBot.setLoginUser(data)
			}).error(function(error){

				YDChatBot.showError(error, true)
			})
    	},
    	showError : function(response, social){

    		YDChatBot.attempts = YDChatBot.attempts + 1;
    		var responseText = response.responseText;
			var responseJson = JSON.parse(responseText) ;
			var errorMessage = responseJson.message ;
			var errorType    = responseJson.type ;
			var data = {}
			if(errorType == "BLOCKED"){
				
				data["text"] = "We have noticed some unusual activites from this account. Please <a href='mailto:customersupport@yourdost.com'>contact us</a> to learn more. ";
			}else{

				data["text"] = "Something went wrong. Please try again later."
			}
			if(social){

				data["text"] = "The username and password do not match";
			}
			data["class"] = "cb-login"
			data["from"] = "Donna"

			YDChatBot.sendToFirebase(data)
            if(YDChatBot.attempts <2){

    			if(social){

    				YDChatBot.showUsername()
    			}else{

    				YDChatBot.saveMessage("login")
    			}
            }else{

                
            }
    	},
    	sendToFirebase : function(data){

    		if( data && typeof data.class != "undefined")$("."+data.class).remove()
    		YDChatBot.messagesRef.push({
		    	"response"   : data.text,
		      	"chatId" : YDChatBot.chatId,
		      	"from": data.from
		    }).then(function() {

		    }).catch(function(error) {

		    	console.error('Error writing new message to Firebase Database', error);
		    });
    	},

    	showPassword : function(){

    		var pswdForm = '<div class="cb-login"><div class="cb-pswd"><input type="password" placeholder="Password" name="pswd" id="cbh-pswd"><button class="cbh-login login">send</button></div></div>';
    		$(".chat-content").append(pswdForm);
    		YDChatBot.bindEvents()
    		$(".chat-content")[0].scrollTop = $(".chat-content")[0].scrollHeight;
    	},
    	showUsername : function(){

    		$(".actionable").remove()
    		var userForm = '<div class="cb-login"><div class="cb-name"><input type="text" name="uname" placeholder="Username" id="cbh-uname"><button class="cbh-login">send</button></div></div>';
    		$(".chat-content").append(userForm);
    		YDChatBot.bindEvents()
    		$(".chat-content")[0].scrollTop = $(".chat-content")[0].scrollHeight;
    	},
    	sendResponse : function(e,src){

    		var data = $(e.currentTarget).attr("data-feeling")
    		if(src) data = src
    		if(data) $(".actionable").remove()

    		YDChatBot.messagesRef.push({
		    	"response"   : data,
		      	"chatId" : YDChatBot.chatId,
		      	"from":"user"
		    }).then(function() {

		    	YDChatBot.saveMessage(data)
		    }).catch(function(error) {

		    	console.error('Error writing new message to Firebase Database', error);
		    });
    	},
    	minimiseChatBot: function(e){

    		if($(e.currentTarget).hasClass("minimise")){

    			$(e.currentTarget).removeClass("minimise")
    			$(".chat-content").show()
    		}else{

    			$(".cb-min").removeClass("minimise")
    			$(e.currentTarget).addClass("minimise")
    			$(".chat-content").hide()
    		}
    	},
    	saveMessage : function(txt){

    		var userId = '';
    		var isLoggedIn = false;
    		if(YDChatApiHandler.getUserId()){

    			userId = JSON.parse(YDChatApiHandler.getUserId()).id
    		}
    		if(YDChatApiHandler.getIsLoggedIn()){

    			isLoggedIn = true
    		}
		    $.ajax({
		        url : YDChatApiHandler.nodeUrl+"/chat",
		        method : 'POST',
		        data:JSON.stringify({'text':txt,'chatID':YDChatBot.chatId,'isLoggedIn':isLoggedIn,'userID':userId}),
		        contentType: "application/json"
		    }).done(function(elem){
		            
		        console.log("Response: ", elem)
		    }).fail(function(error){ 

		        console.log("Error: ", error)
		    });
    	},

    	bindEvents: function() {

			if( typeof this.events !== undefined ){

				for (var key in this.events) {

					if (this.events.hasOwnProperty(key)) {

						var lValue = key.split(" "),elEvent = lValue[0],element = lValue[1],fnstring = this.events[key];
						var fn = this[fnstring];
						if (typeof fn === "function"){

							var el = $(element);
							if( el != null ){

								el.on(elEvent, fn);
							}
						}else{

							console.error(fnstring+" does not exist");
						}						
    				}
 				}
			}
		}
  	}
  	var YDChatApiHandler = {

  		apiUrl: "",
  		nodeUrl : "",
  		username : "",
  		password : "",

  		init : function(app){

  			var config = {

  				"staging" : "https://staging-app.yourdost.com/zion",
  				"local" : "http://local.yourdost.com:9010",
  				"production" : "https://staging-app.yourdost.com/zion"
  			}
  			this.nodeUrl = config[app.environment]
  			this.apiUrl = config.staging
  		},

  		setUsername : function(data){

  			this.username = data;
  		},

  		setPassword : function(data){

  			this.password = data;
  		},

  		getUserNamePswd : function(){

  			return this.username+"-"+this.password
  		},
  		
  		getIsLoggedIn : function(){

  			return localStorage.getItem("isLoggedIn")
  		},

  		getUserId : function(){

  			return localStorage.getItem("user")
  		}
	}

	return YDChatBot;

})($, window)

$(document).ready(function(){

	var appConfig ={

		environment: "local",
		fbId  : "694343250666151",
		gplusId : "10023995707-7jmgui5dj9ufnhg6ft58r32tgjcr42l8.apps.googleusercontent.com"
	};

	var fireBaseConfig = {

	    apiKey: "AIzaSyBx68LXZoha5f4gbgCecuxcaJp5xwfCA2E",
	    authDomain: "chatbot-b64e7.firebaseapp.com",
	    databaseURL: "https://chatbot-b64e7.firebaseio.com",
	    storageBucket: "chatbot-b64e7.appspot.com",
	};

	var botConfig = {

		name : "Donna",
		imageUrl : "http://res.cloudinary.com/http-yourdost-com/image/upload/v1453742585/logo_tttt-01_iqwben.png"
	}

	YDChatWidget.init(appConfig, fireBaseConfig, botConfig)
})